import subprocess

std_in = ["key", "another key", "key is a lie", "key, but cooler", "negative key"]
std_out = ["check", "value", "value does not exist", "funny value", "what"]

print("Starting tests...")
results = []

for index, (i, o) in enumerate(zip(std_in, std_out), start=1):
    print("\n\tTest #{}".format(index))
    process = subprocess.Popen(['./app'],
                               stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE,
                               universal_newlines=True)
    output, error = process.communicate(input=i)
    expected_output = o

    if output.strip() == expected_output:
        result = True
        results.append('.')
    else:
        result = False
        results.append('F')
    print('Input: {}\nExpected: {}\nOutput: {}\nError: {}'.format(i, expected_output, output, error))
    print('Test passed successfully!' if result else 'Test failed!')
    process.terminate()

print_results = ''.join(results)
print("\nResults: ", print_results)
print('All tests passed successfully' if all(results) else 'FAILURE')

