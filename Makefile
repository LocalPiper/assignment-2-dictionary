ASM = nasm
ASMFLAGS = -felf64 -g
LD=ld

program: main.o dict.o lib.o
	$(LD) -o $@ $^

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

lib.o: lib.asm

main.o: main.asm lib.inc dict.inc

dict.o: dict.asm lib.inc

clean:
	$(RM) *.o
	$(RM) program

test: main.o
	python3 test.py

.PHONY: clean