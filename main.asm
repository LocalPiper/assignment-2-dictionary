%include "words.inc"
%include "lib.inc"
%include "dict.inc"
%define MAX_SIZE 255
%define SHIFT_TO_DATA 8
section .data
    msg_found: db "Key found!", 0x0
    msg_not_found: db "Key not found!", 0x0

global _start
section .text

read_text:
    push r9
    xor r9, r9 ; будем использовать его для перемещения по буферу
    .loop:
        ; считываем символ
        call read_char

        ; поскольку функция возвращает 0 если конец потока, проверяем на 0
        test rax, rax
        jz .done

        mov byte[buffer+r9], al ; загоняем считанный символ в массив
        inc r9 ; увеличиваем счетчик

        jmp .loop

    .done:
        ; нуль-терминатор не нужен, если весь остальной массив заполнен нулями
        pop r9 ; вернем исходное значение регистра
        ret

_start:
    call read_text

    mov rdi, buffer
    mov rsi, third_word

    call find_word

    test rax, rax
    jz .not_found

    push rax
    mov rdi, msg_found
    call print_string

    pop r8

    mov rdi, r8

    add rdi, SHIFT_TO_DATA

    call string_length

    add rax, SHIFT_TO_DATA
    add rax, r8
    inc rax

    mov rdi, rax
    call print_string

    jmp .done

    .not_found:
        mov rdi, msg_not_found
        call print_string

    .done:
        call print_newline

        mov rdi, 0
        call exit

section .bss ; обычно размещается в конце программы
    buffer: times MAX_SIZE resb 0 ; массив длины 255, заполненный нулями